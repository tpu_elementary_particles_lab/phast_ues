#include <iostream>
#include <cmath>
#include "TH1.h"
#include "TH2.h"
#include "TMath.h"
#include "TProfile.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "PaAlgo.h"
#include "G3part.h"

// 502 dimuon with 
// XX0>30 tracks 
// dimuon trigger
// Z first < 300 cm and Z last > 1500 cm
// time of muon tracks is defined
// ∆t μ < 5 ns
// Chi2 < 10

bool checkOTTrigger(const PaTrack& track, const PaTPar& Param);
bool checkMTTrigger(const PaTrack& track, const PaTPar& Param);
bool checkLASTTrigger(const PaTrack& track, const PaTPar& Param);
bool checkHodoPlane(string TBName, const PaTrack& track, const PaTPar& cParam);


void UserEvent56(PaEvent& e)
{

  const double M_mu = G3partMass[5];  // muon   mass
  const double M_pi = G3partMass[8];  // pion   mass
  const double M_Pr = G3partMass[14]; // proton mass

  static TH1D*     hist[14];  
  //static TTree* tree(NULL);

  const int bestPvi = e.iBestPrimaryVertex();

  //---------------
  static bool first(true); // when we meet the first event; there is EndJob, EndRun, but there isn't Start...
  if(first){ // histograms and Ntupes booking block
    //Phast::Ref().HistFileDir("UserEvent501");

    hist[0]   = new TH1D("h_chu0","all dimuon candidates", 200, 0, 10);
    hist[1]   = new TH1D("h_chu1","XX0>30 tracks", 200, 0, 10);
    hist[2]   = new TH1D("h_chu2","dimuon trigger", 200, 0, 10);
    hist[3]   = new TH1D("h_chu3","Z first < 300 cm and Z last > 1500 cm", 200, 0, 10);
    hist[4]   = new TH1D("h_chu4","time of muon tracks is defined", 200, 0, 10);
    hist[5]   = new TH1D("h_chu5","#Delta t < 5 ns", 200, 0, 10);
    hist[6]   = new TH1D("h_chu6","#Chi^2 < 10", 200, 0, 10);
    hist[7]   = new TH1D("h_chu7","Trigger validation", 200, 0, 10);
    hist[8]   = new TH1D("h_chu8","Only good spills", 200, 0, 10);
    hist[9]   = new TH1D("h_chu9","x-variables cut", 200, 0, 10);
    hist[10]  = new TH1D("h_chu10","q_T cut", 200, 0, 10);
    hist[11]  = new TH1D("h_chu11","Z of primary vertex", 200, 0, 10);
    hist[12]  = new TH1D("h_chu12","Radial cut", 200, 0, 10);


    first=false;
  } // end of histogram booking
  //---------------

    // loop over reconstructed tracks, select with mu+ particles
    for(int it = 0; it < e.NTrack(); it++) {
        const PaTrack& itrack = e.vTrack(it);
        int ip = itrack.iParticle();
        if (ip == -1) continue;
        const PaParticle& mu_p = e.vParticle(ip);
        if(mu_p.Q() != 1) continue;
        
        // loop over reconstructed tracks, select with mu- particles
        for(int jt = 0; jt < e.NTrack(); jt++) {  
            const PaTrack& jtrack = e.vTrack(jt);
            int jp = jtrack.iParticle();
            if (jp == -1) continue;
            const PaParticle& mu_m = e.vParticle(jp);
            if(mu_m.Q() != -1) continue;  
        
            int suitableVi = -1;
            float vChi2 = -1.;
            bool bFindBestPv = false;
            // loop over the mu+ verteces
            for(int i = 0; i < mu_p.NVertex(); i++) {
                int iv = mu_p.iVertex(i);   // get vertex index
                const PaVertex& vp = e.vVertex(iv);   
                //if (!vp.IsPrimary()) continue;
                
                // loop over the mu- verteces                
                for(int j = 0; j < mu_m.NVertex(); j++) {
                    int jv = mu_m.iVertex(j); 
                    if (jv != iv) continue; // check if current index the same as for mu+
                    const PaVertex& vm = e.vVertex(jv);
                    
                    // looking for appropriate vertex (index for this vertex is defined in suitableVi variable)
                    if (jv != bestPvi) {
                    //if (!vm.IsBestPrimary()) {
                    if (!vp.IsPrimary()) continue;
                        if (vChi2 > vm.Chi2() or vChi2 == -1.) {
                             suitableVi = jv;
                             vChi2 = vm.Chi2();
                        }
                    } else {
                        suitableVi = jv;
                        bFindBestPv = true;
                        break;
                    }
                    
                    // end of looking for appropriate vertex
                    
                }  // end of loop over the mu- verteces
                if (bFindBestPv == true) break;
            } // end of loop over the mu+ verteces
            if (suitableVi == -1) continue;
            
            
            // define parameters
            const PaTPar& parMp = mu_p.ParInVtx(suitableVi);
            const PaTPar& parMm = mu_m.ParInVtx(suitableVi);
                     
            //float Minv = sqrt(PaAlgo::Q2(parMp.LzVec(M_mu), parMm.LzVec(M_mu)));
            float Minv = (parMp.LzVec(M_mu)+ parMm.LzVec(M_mu)).M();
            
            if ((Minv > 4.3 and Minv < 8.5) == false) continue;
            hist[0]->Fill(Minv);
            
            e.TagToSave();
            
            
            // CUTS
            if ((jtrack.XX0() < 30) or (itrack.XX0() < 30)) continue;
            hist[1]->Fill(Minv);

            // dimuon trigger cut for DY
            int trig = e.TrigMask()&2047;
            int trigBit_LASTLAST = (trig&1 << 8);
            int trigBit_LASTOT = (trig&1 << 2);
            int trigBit_LASTMT = (trig&1 << 0);
            if (trigBit_LASTLAST == 0 && trigBit_LASTOT == 0) continue;
            if (trigBit_LASTMT != 0) continue; // LAST+MT
            hist[2]->Fill(Minv);
            
            // Z point cut
            if (((itrack.ZFirst()<300 and itrack.ZLast()>1500) and (jtrack.ZFirst()<300 and jtrack.ZLast()>1500)) == false) continue;
            hist[3]->Fill(Minv);
            
            // time of muon tracks is defined cut
            // if (trackb.MeanTime() == TMath::Power(10,10)) continue;
            if ((itrack.SigmaTime() == 0 or jtrack.SigmaTime() == 0) == true) continue;
            hist[4]->Fill(Minv);
                      
            // ∆t μ < 5 ns cut
            if ((abs(itrack.MeanTime() - jtrack.MeanTime()) < 5) == false) continue;
            hist[5]->Fill(Minv);
        
            // Chi2<10 cut
            if (((itrack.Chi2tot()/itrack.Ndf()<10) && (jtrack.Chi2tot()/jtrack.Ndf()<10))==false) continue;
            hist[6]->Fill(Minv);
            
            
            // trigger validation cut
            // itrack corresponds to mu+, jtrack corresponds to mu-
            bool trigValidLASTp = checkLASTTrigger(itrack, parMp);
            bool trigValidLASTm = checkLASTTrigger(jtrack, parMm);
            
            bool trigValidOTp = checkOTTrigger(itrack, parMp);
            bool trigValidOTm = checkOTTrigger(jtrack, parMm);
            
            //bool trigValidMTp = checkMTTrigger(itrack, parMp);
            //bool trigValidMTm = checkMTTrigger(jtrack, parMm);
            
            bool trigValLAST_OT = ((trigValidOTp && trigValidLASTm) or (trigValidOTm && trigValidLASTp)) && (trigBit_LASTOT != 0);
            bool trigValLAST    = trigValidLASTp && trigValidLASTm && (trigBit_LASTLAST != 0);
            //bool trigValLAST_MT = ((trigValidMTp && trigValidLASTm) or (trigValidMTm && trigValidLASTp)) && (trigBit_LASTMT != 0); // LAST+MT excluded by dimuon trigger bit cut

            // if (trigValLAST_MT == true) continue; // trying to reject LAST+MT events
            if ((trigValLAST_OT or trigValLAST) == false) continue;            
            hist[7]->Fill(Minv);
            
            TLorentzVector photonVect = parMp.LzVec(M_mu)+ parMm.LzVec(M_mu); // virtual photon 4-vector
            TLorentzVector protonVect(0, 0, 0, M_Pr);  // proton's 4-vector (in laboratory ref. frame), components (Px, Py, Pz, E)
            
            //cout << "photon vect (" << photonVect(0) << ", " << photonVect(1) << ", " << photonVect(2) << ", " << photonVect(3) << ")" << endl;
            
            PaVertex vertex = e.vVertex(suitableVi);
            PaParticle pion = e.vParticle(vertex.InParticle());
            PaTPar pionPar = pion.ParInVtx(suitableVi);
            TLorentzVector pionVect = pionPar.LzVec(M_pi);
            
            //cout << "pion vect (" << pionVect(0) << ", " << pionVect(1) << ", " << pionVect(2) << ", " << pionVect(3) << ")" << endl;
            
            double xPion   = ((photonVect*photonVect)/(2*pionVect*photonVect));
            double xProton = ((photonVect*photonVect)/(2*protonVect*photonVect));
            double xFeynman = xPion - xProton;
            
            if (xPion < 0 or xPion > 1) continue;
            if (xProton < 0 or xProton > 1) continue;
            if (xFeynman < -1 or xFeynman > 1) continue;
            hist[9]->Fill(Minv);
            
            // cut for transverse component of the virtual photon momentum, 0.4 < q_T < 5.0 GeV/c

            Double_t angleYZ = atan(pionVect.Y()/pionVect.Z());
            pionVect.RotateX(angleYZ);
            photonVect.RotateX(angleYZ);

            //cout << "pion vector Y " << pionVect.Y() << endl;

            Double_t angleXZ = -atan(pionVect.X()/pionVect.Z());
            pionVect.RotateY(angleXZ);
            photonVect.RotateY(angleXZ);

            //cout << "pion vector X " << pionVect.X() << endl;
             
            Double_t alpha = -atan(photonVect.Y()/photonVect.X());
            photonVect.RotateZ(alpha);


            Double_t qTrans = photonVect.Pt();
            cout << "q_T " << qTrans << endl;
            if (qTrans < 0.4 or qTrans > 5.0) continue;
            hist[10]->Fill(Minv);
            
            // z-position of the primary vertex cut
            // from Yu-Shiang's title of event flow Z_{vtx}\in[-295,-240]~||~[-220,-165] cm
            //if ((((-294.5 < vertex.Z()) and (vertex.Z() < -239.3)) or ((-219.5 < vertex.Z()) and (vertex.Z() < -164.3))) == false) continue;
            if ((((-295 < vertex.Z()) and (vertex.Z() < -240)) or ((-220 < vertex.Z()) and (vertex.Z() < -165))) == false) continue;
            hist[11]->Fill(Minv);
            
            // radial cut
            if ((sqrt(vertex.X()*vertex.X() + vertex.Y()*vertex.Y()) < 1.9) == false) continue;
            hist[12]->Fill(Minv);


        } //  end of loop over reconstructed tracks, select with mu- particles
        
     } // end of loop over reconstructed tracks, select with mu+ particles

}



bool checkHodoPlane(string TBName, const PaTrack& track, const PaTPar& cParam) {
    PaTPar Param = cParam; // define non constant parameters
    //cout << "current TBName is " << TBName << endl;
    const PaDetect& HOD = PaSetup::Ref().Detector(PaSetup::Ref().iDetector(TBName));
    float zcoord = HOD.Z();
    track.Extrapolate(zcoord, Param);
    return HOD.InActive(Param.X(),Param.Y());
                
}

bool checkOTTrigger(const PaTrack& track, const PaTPar& Param) {
    //     For OT: “HO03Y1_m”, “HO04Y1_m”, “HO04Y2_m” (HO03 in upstream, HO04 in downstream)
    bool upStreamOT     = checkHodoPlane("HO03Y1_m", track, Param);
    bool downStreamOT_1 = checkHodoPlane("HO04Y1_m", track, Param);
    bool downStreamOT_2 = checkHodoPlane("HO04Y2_m", track, Param);

    bool resOT = upStreamOT && (downStreamOT_1 or downStreamOT_2);
    
    return resOT;
    
}


bool checkLASTTrigger(const PaTrack& track, const PaTPar& Param) {
    //For LAST: “HG01Y1__”, “HG02Y1__”, “HG02Y2” (HG01 in upstream, HG02 in downstream)
    bool upStreamLAST     = checkHodoPlane("HG01Y1__", track, Param);
    bool downStreamLAST_1 = checkHodoPlane("HG02Y1__", track, Param);
    bool downStreamLAST_2 = checkHodoPlane("HG02Y2__", track, Param);
    
    bool resLAST = upStreamLAST && (downStreamLAST_1 or downStreamLAST_2);

    return resLAST;
    
}


bool checkMTTrigger(const PaTrack& track, const PaTPar& Param) {
    // For MT: “HM04Y1_d”, “HM05Y1_d”, “HM04Y1_u”, “HM05Y1_u” (HM04 in upstream, HM05 in downstream)
    bool upStreamMT_1   = checkHodoPlane("HM04Y1_d", track, Param);
    bool upStreamMT_2   = checkHodoPlane("HM04Y1_u", track, Param);
    bool downStreamMT_1 = checkHodoPlane("HM05Y1_d", track, Param);
    bool downStreamMT_2 = checkHodoPlane("HM05Y1_u", track, Param);
    
    bool resMT = (upStreamMT_1 or upStreamMT_2) && (downStreamMT_1 or downStreamMT_2);
    
    return resMT;
    
}





